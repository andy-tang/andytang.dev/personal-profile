# Personal Profile with Next.js

| Service Name | personal-profile                                                                          |
| ------------ | ----------------------------------------------------------------------------------------- |
| Language     | ![](https://img.shields.io/badge/Next.js-v10.2-000000?logo=next-dot-js&style=flat-square) |
| Requisites   | ![](https://img.shields.io/badge/SMTP--Server-000000?logo=gmail&style=flat-square)        |
| Service Host | Prod: [https://andytang.dev](https://demo.andytang.dev)                                   |

## Keywords

- Javascript: `TypeScript`, `Next.js`, `React.js`, `Node.js`, `Nodemailer`
- Frontend: `Sass`, `JSS`, `Material-UI`, `reCAPTCHA`, `PWA`
- DevOps: `Docker`, `Kubernetes`, `GitLab CI/CD`
- Others: `Google Geocoding API`

## Overview

This is the project I used for my personal profile. :D

Most website contents, including the intro, career and contact page, are configurable with JSON files.

A contact form is provided on this website to receive messages from visitors.

## Quick Start

```bash
npm run dev
# or
yarn dev
```

