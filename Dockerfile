# syntax = docker/dockerfile:experimental
#--------------------------------------------------------------------------------
# Stage 1: Compile Typescript and Next.js
#--------------------------------------------------------------------------------
FROM node:lts-alpine as builder
LABEL stage=intermediate
ENV NEXT_TELEMETRY_DISABLED 1

WORKDIR /opt/app
COPY package.json yarn.lock ./
RUN --mount=type=cache,target=/opt/app/.cache/yarn \
    yarn config set cache-folder /opt/app/.cache/yarn && \
    yarn install --frozen-lockfile

COPY . .
RUN yarn build

#--------------------------------------------------------------------------------
# Stage 2: Prepare running environment for Next.js container
#--------------------------------------------------------------------------------
FROM node:lts-alpine

WORKDIR /opt/app
ENV NODE_ENV production
COPY --from=builder /opt/app/next.config.js .
COPY --from=builder /opt/app/public ./public
COPY --from=builder /opt/app/.next ./.next
COPY --from=builder /opt/app/node_modules ./node_modules
COPY --from=builder /opt/app/package.json ./package.json

EXPOSE 3000
CMD ["yarn", "start"]
