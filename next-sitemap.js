module.exports = {
  siteUrl: process.env.SITE_HOST,
  generateRobotsTxt: true,
}
