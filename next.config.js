const withPlugins = require('next-compose-plugins')
const withTM = require('next-transpile-modules')(['ansi-regex', 'debug'])
const withImages = require('next-images')
const withPWA = require('next-pwa')
const runtimeCaching = require('next-pwa/cache')

module.exports = withPlugins(
  [
    [withTM],
    [withImages],
    [
      withPWA,
      {
        pwa: {
          dest: 'public',
          disable: process.env.NODE_ENV === 'development',
          runtimeCaching,
        },
      },
    ],
  ],
  {
    future: {
      webpack5: true,
    },
    webpack(config) {
      config.module.rules.push({
        test: /\.md$/,
        use: 'raw-loader',
      })

      return config
    },
  }
)
