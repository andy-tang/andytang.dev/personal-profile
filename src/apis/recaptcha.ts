import axios from 'axios'
import qs from 'qs'

const secret = process.env.RECAPTCHA_SECRET_KEY

export const apiVerifyRecaptcha = (data: { response: string }) =>
  axios.post(
    'https://www.google.com/recaptcha/api/siteverify',
    qs.stringify({
      secret,
      ...data,
    }),
    {
      headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8' },
    }
  )
