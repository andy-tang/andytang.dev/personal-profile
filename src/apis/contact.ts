import axios from 'axios'

export const apiSendContactForm = (data: {
  token: string | null
  name: string
  email: string
  phone?: number
  message: string
}) => axios.post('/api/contact', data)
