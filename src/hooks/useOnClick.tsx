import { useRouter } from 'next/router'

export const useOnClick: () => (e: React.MouseEvent<HTMLElement>) => void = () => {
  const router = useRouter()

  return (e: React.MouseEvent<HTMLElement>) => {
    const elm = e.target as HTMLElement
    const href = elm?.getAttribute('href')
    if (href?.startsWith('/')) {
      e.preventDefault()
      router.push(href)
    } else {
      elm?.setAttribute('target', '_blank')
    }
  }
}

export default useOnClick
