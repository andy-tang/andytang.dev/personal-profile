import { useRouter } from 'next/router'
import { pages } from '../assets/json/pages'
import { pathToRegexp } from 'path-to-regexp'

export interface Page {
  label: string
  path?: string
  title?: string
  enabled: boolean
  fullWidth?: boolean
}

export const usePage = (): Page => {
  const router = useRouter()
  const pageNotFount = {
    label: 'Hey, there is nothing here',
    path: undefined,
    title: 'Nothing',
    enabled: false,
  }
  return pages.find(({ path }) => pathToRegexp(path).exec(router.pathname)) ?? pageNotFount
}

export default usePage
