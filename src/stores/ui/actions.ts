import { AnyAction } from 'redux'
import { Types } from './types'

export const openBackdrop = (open: boolean): AnyAction => {
  return {
    type: Types.OPEN_BACKDROP,
    payload: open,
  }
}
