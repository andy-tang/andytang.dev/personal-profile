import { AnyAction } from 'redux'
import { Types } from './types'

export interface IUIState {
  backdrop: { open: boolean }
}

const INITIAL_STATE: IUIState = {
  backdrop: { open: false },
}

export const uiReducer = (state: IUIState = INITIAL_STATE, action: AnyAction): IUIState => {
  switch (action.type) {
    case Types.OPEN_BACKDROP:
      return { ...state, backdrop: { open: action.payload } }
    default:
      return state
  }
}
