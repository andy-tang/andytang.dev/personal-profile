import { AnyAction, combineReducers } from 'redux'
import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import { IUIState, uiReducer } from './ui/reducer'

// export interface IRootState {
//   ui: IUIState
// }

const reducers = combineReducers({
  ui: uiReducer,
})

const rootReducer = (state: any, action: AnyAction) => {
  if (action.type === 'RESET_REDUX') {
    // TBC
  }
  return reducers(state, action)
}

const persistConfig = {
  key: 'root',
  storage,
  whitelist: [],
}

export type RootState = ReturnType<typeof reducers>
export default persistReducer(persistConfig, rootReducer)
