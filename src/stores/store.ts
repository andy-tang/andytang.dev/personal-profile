import { applyMiddleware, createStore } from 'redux'
import { persistStore } from 'redux-persist'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'
import logger from 'redux-logger'
import reducers from './reducers'

export type StoreTypes = ReturnType<typeof reducers>

const composeEnhancers = composeWithDevTools({
  trace: true,
  traceLimit: 25,
})

const store = createStore(reducers, composeEnhancers(applyMiddleware(...[logger, thunk])))
const persistor = persistStore(store)

export { store, persistor }
