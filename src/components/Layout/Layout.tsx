import React, { useState } from 'react'
import { useRouter } from 'next/router'
import { useOnClick, usePage } from '../../hooks'
import { pathToRegexp } from 'path-to-regexp'
import systemConfig from '../../assets/json/system-config.json'
import { pages } from '../../assets/json/pages'

import Head from 'next/head'
import {
  Container,
  Divider,
  Grid,
  Hidden,
  IconButton,
  Paper,
  Slide,
  Tab,
  Tabs,
  useScrollTrigger,
} from '@material-ui/core'
import withWidth, { isWidthUp } from '@material-ui/core/withWidth'
import { Breakpoint } from '@material-ui/core/styles/createBreakpoints'
import { Menu as MenuIcon } from '@material-ui/icons'

import Logo from '../Logo/Logo'
import Sidebar from '../Sidebar/Sidebar'
import MobileMenu from '../Menu/MobileMenu'
import Backdrop from '../Backdrop/Backdrop'
import styles from './Layout.module.scss'

const Layout = ({ children, width }: { children: JSX.Element; width: Breakpoint }) => {
  const page = usePage()
  const direction = isWidthUp('md', width) ? 'row' : 'column-reverse'

  return (
    <>
      <Head>
        <title>{!!page.title ? `${systemConfig.site.title} - ${page.title}` : systemConfig.site.title}</title>
      </Head>
      <Background />
      <Header />
      {page.fullWidth ? (
        children
      ) : (
        <Container maxWidth="xl" className={styles.main}>
          <Grid container direction={direction} spacing={4} wrap="nowrap" className={styles.container}>
            <Grid item md={4} className={styles.sidebar}>
              <Sidebar />
            </Grid>
            <Grid item md={8} className={styles.page}>
              <div>
                {page.path !== '/' && (
                  <>
                    <div className={styles.title}>
                      <h1>{page.label}</h1>
                    </div>
                    <Divider style={{ margin: '0 0 3em' }} />
                  </>
                )}
                <div>{children}</div>
              </div>
            </Grid>
          </Grid>
        </Container>
      )}
      <Footer />
      <Backdrop />
    </>
  )
}

const Header = () => {
  const router = useRouter()
  const trigger = useScrollTrigger()
  const [openMenu, setOpenMenu] = useState(false)
  const tabIndex = pages.findIndex(({ path }) => pathToRegexp(path).exec(router.pathname))

  return (
    <Slide appear={false} direction="down" in={!trigger}>
      <Paper elevation={1} square className={styles.header}>
        <Container maxWidth="xl">
          <Grid container direction="row" justify="space-between" alignItems="center" className={styles.container}>
            <Grid item onClick={() => router.push('/')}>
              <Logo label={`andytang.dev`} />
            </Grid>

            <Hidden smDown>
              <Grid item xs>
                <Tabs value={tabIndex > -1 ? tabIndex : 0} TabIndicatorProps={{ style: { display: 'none' } }}>
                  {pages.map(({ label, path, enabled }, index) => (
                    <Tab label={label} onClick={() => router.push(path)} disabled={!enabled} key={index} />
                  ))}
                </Tabs>
              </Grid>
            </Hidden>
            <Hidden mdUp>
              <IconButton edge="end" color="inherit" aria-label="menu" onClick={() => setOpenMenu(true)}>
                <MenuIcon />
              </IconButton>
              <MobileMenu open={openMenu} onClose={() => setOpenMenu(false)} pages={pages} />
            </Hidden>
          </Grid>
        </Container>
      </Paper>
    </Slide>
  )
}

const Footer = () => {
  const onClick = useOnClick()

  return (
    <div className={styles.footer}>
      <p>© 2021 Copyright andytang.dev. All rights reserved.</p>
      <ul onClick={onClick}>
        <li>
          <a href="/about#terms-of-use">Terms of Use</a>
        </li>
        <li>
          <a href="/about#privacy-policy">Privacy Policy</a>
        </li>
      </ul>
    </div>
  )
}

const Background = () => {
  return <Paper className={styles.background} />
}

export default withWidth(styles)(Layout)
