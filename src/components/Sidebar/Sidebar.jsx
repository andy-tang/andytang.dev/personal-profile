import React from 'react'
import classNames from 'classnames'
import personalInfo from '../../assets/json/personal.json'

import { Avatar, Box, Divider } from '@material-ui/core'
import { ActionButtons, SocialMediaBar } from '../../components'
import styles from './Sidebar.module.scss'

export default function Sidebar() {
  return (
    <div className={styles.sidebar}>
      <div className={styles.info}>
        <Avatar alt="avatar" src={require('../../assets/images/avatar.svg')} className={styles.avatar} />
        <p className={styles.name}>{personalInfo.contact.name}</p>
        <p className={classNames('desc', styles.email)}>
          <a href={`mailto:${personalInfo.contact.email}`}>{personalInfo.contact.email}</a>
        </p>
      </div>
      <Box component="div" p="2em 0">
        <SocialMediaBar />
      </Box>
      <Divider style={{ margin: '4em 0' }} />
      <div className={styles.welcome}>
        <p>Hello!</p>
        <p>Welcome to my personal website.</p>
      </div>
      <ActionButtons />
    </div>
  )
}
