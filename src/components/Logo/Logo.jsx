import React from 'react'

import styles from './Logo.module.scss'

export default function Logo({ label, onClick = undefined }) {
  return (
    <div className={styles.lego} onClick={onClick}>
      <span>{label}</span>
    </div>
  )
}
