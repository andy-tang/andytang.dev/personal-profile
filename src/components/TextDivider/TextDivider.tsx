import React from 'react'
import { makeStyles } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    alignItems: 'center',
  },
  border: {
    borderBottom: '1px solid rgba(0, 0, 0, 0.12)',
    width: '100%',
  },
  content: {
    paddingTop: theme.spacing(0.5),
    paddingBottom: theme.spacing(0.5),
    paddingRight: theme.spacing(2),
    paddingLeft: theme.spacing(2),
  },
}))

export default function TextDivider({ children }: { children: JSX.Element }) {
  const styles = useStyles()
  return (
    <div className={styles.container}>
      <div className={styles.border} />
      <span className={styles.content}>{children}</span>
      <div className={styles.border} />
    </div>
  )
}
