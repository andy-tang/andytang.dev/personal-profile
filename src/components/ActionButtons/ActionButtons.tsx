import React from 'react'
import { useRouter } from 'next/router'

import { Button, Grid } from '@material-ui/core'
import styles from './ActionButtons.module.scss'

export default function ActionButtons() {
  const router = useRouter()

  return (
    <Grid container spacing={2} className={styles.actionButtons}>
      <Grid item xs={12} md={6}>
        <Button size="small" variant="outlined" color="primary" fullWidth onClick={() => router.push('/intro')}>
          About me
        </Button>
      </Grid>
      <Grid item xs={12} md={6}>
        <Button size="small" variant="outlined" color="primary" fullWidth onClick={() => router.push('/projects')}>
          My projects
        </Button>
      </Grid>
    </Grid>
  )
}
