import React from 'react'
import { useRouter } from 'next/router'
import { Page } from '../../hooks/usePage'

import { Divider, Drawer, IconButton, List, ListItem } from '@material-ui/core'
import { Close as CloseIcon } from '@material-ui/icons'
import styles from './MobileMenu.module.scss'

export default function MobileMenu({
  open,
  onClose,
  pages,
}: {
  open: boolean | undefined
  onClose: React.MouseEventHandler<HTMLButtonElement> | undefined
  pages: Page[]
}) {
  const router = useRouter()
  return (
    <Drawer open={open} onClose={onClose} anchor="left">
      <div className={styles.mobileMenu}>
        <div className={styles.header}>
          <IconButton edge="end" color="inherit" aria-label="close" onClick={onClose}>
            <CloseIcon />
          </IconButton>
        </div>
        <Divider />
        <List>
          {pages.map(({ label, path }, index: React.Key) => (
            <ListItem
              button
              onClick={() => {
                !!path && router.push(path)
                onClose?.call(null)
              }}
              key={index}
            >
              <span className={styles.label}>{label}</span>
            </ListItem>
          ))}
        </List>
      </div>
    </Drawer>
  )
}
