import { useRef, useLayoutEffect, LegacyRef } from 'react'

import * as am4maps from '@amcharts/amcharts4/maps'
import * as am4core from '@amcharts/amcharts4/core'
import am4themes_animated from '@amcharts/amcharts4/themes/animated'
import am4geodata_worldUltra from '@amcharts/amcharts4-geodata/worldUltra'
import styles from './TravelMap.module.scss'

am4core.useTheme(am4themes_animated)

export default function TravelMap({
  mapData,
}: {
  mapData: {
    [country: string]: { [city: string]: { long: number; lat: number; dates: string[] } }
  }
}) {
  const ref = useRef<HTMLElement>()

  const makeImageSeries = (chart: am4maps.MapChart) => {
    const imageSeries = chart.series.push(new am4maps.MapImageSeries())
    const imageSeriesTemplate = imageSeries.mapImages.template
    const circle = imageSeriesTemplate.createChild(am4core.Circle)
    circle.radius = 4
    circle.stroke = am4core.color('#FFFFFF')
    circle.strokeWidth = 1
    circle.nonScaling = true
    circle.tooltipText = '{label}'
    imageSeriesTemplate.propertyFields.longitude = 'long'
    imageSeriesTemplate.propertyFields.latitude = 'lat'
    imageSeriesTemplate.propertyFields.fill = 'fill'
    return imageSeries
  }

  const makePolygonSeries = (chart: am4maps.MapChart, config: { fill: string }) => {
    const polygonSeries = chart.series.push(new am4maps.MapPolygonSeries())
    polygonSeries.useGeodata = true
    const polygonTemplate = polygonSeries.mapPolygons.template
    polygonTemplate.tooltipText = '{name}'
    polygonTemplate.fill = am4core.color(config.fill)
    polygonTemplate.propertyFields.fill = 'fill'
    return polygonSeries
  }

  useLayoutEffect(() => {
    if (ref.current === null) return

    const chart = am4core.create(ref.current, am4maps.MapChart)
    chart.homeZoomLevel = 1
    chart.geodata = am4geodata_worldUltra
    chart.projection = new am4maps.projections.Miller()
    chart.responsive.enabled = true

    // Chart series
    const polygonSeries = makePolygonSeries(chart, { fill: 'rgba(131, 153, 168, 0.5)' })
    polygonSeries.data = [
      {
        id: 'HK',
        fill: am4core.color('#0063a9'),
      },
      ...Object.keys(mapData).map((id) => {
        return { id, fill: am4core.color('rgba(131, 153, 168, 1)') }
      }),
    ]

    // Visited cities
    const imageSeries = makeImageSeries(chart)
    imageSeries.data = [
      {
        label: 'Hong Kong\nHome',
        long: 114.177216,
        lat: 22.302711,
        fill: '#de5793',
      },
      ...Object.values(mapData)
        .map((value) => Object.entries(value))
        .flat()
        .map(([label, { long, lat, dates }]) => {
          return {
            label: `${label}\n${dates.length} times`,
            long,
            lat,
            fill: '#0063a9',
          }
        })
        .filter(({ long, lat }) => !(long === lat && long === 0)),
    ]

    return () => {
      chart.dispose()
    }
  }, [])

  return (
    <div className={styles.travelMap}>
      <div ref={ref as LegacyRef<HTMLDivElement>} className={styles.chart} />
    </div>
  )
}
