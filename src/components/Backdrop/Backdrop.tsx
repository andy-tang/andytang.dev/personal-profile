import React from 'react'
import { useSelector } from 'react-redux'

import { RootState } from '../../stores'
import { CircularProgress, Backdrop as MuiBackdrop, makeStyles } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}))

export default function Backdrop() {
  const styles = useStyles()
  const { open } = useSelector((state: RootState) => state.ui.backdrop)
  return (
    <MuiBackdrop open={open} onClick={() => {}} className={styles.backdrop}>
      <CircularProgress color="inherit" />
    </MuiBackdrop>
  )
}
