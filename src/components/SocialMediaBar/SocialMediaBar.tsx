import React from 'react'
import personalInfo from '../../assets/json/personal.json'

import { Avatar, Grid } from '@material-ui/core'
import styles from './SocialMediaBar.module.scss'

export const socials: { icon: string; label: string; backgroundColor?: string; onClick?: () => void }[] = [
  {
    icon: require('../../assets/images/contact/email.svg'),
    label: 'Email',
    onClick() {
      window.open(`mailto:${personalInfo.contact.email}`)
    },
  },
  {
    icon: require('../../assets/images/contact/linkedin.svg'),
    label: 'Linkedin',
    onClick() {
      window.open(personalInfo.social.linkedin)
    },
  },
  {
    icon: require('../../assets/images/contact/gitlab.svg'),
    label: 'Gitlab',
    backgroundColor: '#554488',
    onClick() {
      window.open(personalInfo.social.gitlab)
    },
  },
]

export default function SocialMediaBar() {
  return (
    <Grid container justify="center" className={styles.socialMediaBar}>
      {socials.map(({ icon, label, backgroundColor, onClick }, index) => (
        <Grid item onClick={onClick} style={{ margin: '0 1em' }} key={index}>
          <Avatar src={icon} alt={label} style={{ backgroundColor }} />
        </Grid>
      ))}
    </Grid>
  )
}
