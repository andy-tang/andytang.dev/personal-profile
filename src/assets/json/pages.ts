export const pages: { label: string; path: string; title?: string; enabled: boolean; fullWidth?: boolean }[] = [
  { label: 'Home', path: '/', enabled: true, fullWidth: true },
  { label: 'Intro', path: '/intro', title: 'Intro', enabled: true },
  { label: 'Career', path: '/career', title: 'Career', enabled: true },
  { label: 'Projects', path: '/projects', title: 'Projects', enabled: true },
  { label: 'Contact', path: '/contact', title: 'Contact', enabled: true },
  { label: 'About', path: '/about', title: 'About', enabled: true },
]
