import React from 'react'
import { useOnClick } from '../hooks'
import gfm from 'remark-gfm'
import career from '../assets/md/career.md'

import { Grid } from '@material-ui/core'
import ReactMarkdown from 'react-markdown'
import styles from '../styles/pages/Career.module.scss'

export default function Career() {
  const onClick = useOnClick()

  return (
    <Grid container direction="column" wrap="nowrap" className={styles.career}>
      <Grid item onClick={onClick}>
        {<ReactMarkdown remarkPlugins={[gfm]} children={career} />}
      </Grid>
    </Grid>
  )
}
