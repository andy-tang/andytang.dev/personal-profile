import 'react-app-polyfill/ie11'
import 'react-app-polyfill/stable'

import React from 'react'
import Head from 'next/head'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { QueryClient, QueryClientProvider } from 'react-query'
import { SnackbarProvider as NotistackProvider } from 'notistack'
import { persistor, store } from '../stores'
import systemConfig from '../assets/json/system-config.json'

import CssBaseline from '@material-ui/core/CssBaseline'
import { Fade } from '@material-ui/core'
import { createMuiTheme, makeStyles, ThemeProvider } from '@material-ui/core/styles'
import Layout from '../components/Layout/Layout'

import '../styles/globals.scss'

const appTheme = createMuiTheme({
  palette: {
    primary: {
      main: 'rgba(0, 0, 0, 0.87)',
    },
    secondary: {
      main: 'rgba(0, 0, 0, 0.54)',
    },
  },
})

export default function App({ Component, pageProps }: { Component: typeof React.Component; pageProps: any }) {
  return (
    <ReduxProvider>
      <ReactQueryProvider>
        <SnackbarProvider>
          <MetaProvider>
            <ThemeProvider theme={appTheme}>
              <CssBaseline />
              <Layout>
                <Component {...pageProps} />
              </Layout>
            </ThemeProvider>
          </MetaProvider>
        </SnackbarProvider>
      </ReactQueryProvider>
    </ReduxProvider>
  )
}

const SnackbarProvider = ({ children }: { children: JSX.Element }) => {
  const styles = makeStyles((theme) => ({
    containerRoot: {
      [theme.breakpoints.down('xs')]: {
        transform: 'translateX(-50%)',
      },
      '& .MuiCollapse-wrapper': {
        justifyContent: 'center',
      },
    },
  }))()

  return (
    <NotistackProvider
      maxSnack={3}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
      TransitionComponent={Fade}
      classes={styles}
    >
      {children}
    </NotistackProvider>
  )
}

const ReactQueryProvider = ({ children }: { children: JSX.Element }) => {
  const queryClient = new QueryClient()
  return <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>
}

const ReduxProvider = ({ children }: { children: JSX.Element }) => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        {children}
      </PersistGate>
    </Provider>
  )
}

const MetaProvider = ({ children }: { children: JSX.Element }) => {
  return (
    <>
      <Head>
        <base target="_blank"></base>
        <meta name="application-name" content="AndyTang.dev" />
        <meta name="description" content={`${systemConfig.site.title}'s personal website`} />
        <meta name="format-detection" content="telephone=no,email=no,adress=no" />
        <meta name="mobile-web-app-capable" content="yes" />
        <meta name="theme-color" content="#0063a9" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="manifest" href="/manifest.json" />
        <link rel="icon" href="/favicon.ico" />
        <link rel="icon" type="image/png" sizes="32x32" href="/icons/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="96x96" href="/icons/favicon-96x96.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="/icons/favicon-16x16.png" />
        <link rel="canonical" href={window.location.href} />
        {/* Apple */}
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="default" />
        <meta name="apple-mobile-web-app-title" content="AndyTang.dev" />
        <link rel="apple-touch-icon" sizes="57x57" href="/icons/apple-icon-57x57.png" />
        <link rel="apple-touch-icon" sizes="60x60" href="/icons/apple-icon-60x60.png" />
        <link rel="apple-touch-icon" sizes="72x72" href="/icons/apple-icon-72x72.png" />
        <link rel="apple-touch-icon" sizes="76x76" href="/icons/apple-icon-76x76.png" />
        <link rel="apple-touch-icon" sizes="114x114" href="/icons/apple-icon-114x114.png" />
        <link rel="apple-touch-icon" sizes="120x120" href="/icons/apple-icon-120x120.png" />
        <link rel="apple-touch-icon" sizes="144x144" href="/icons/apple-icon-144x144.png" />
        <link rel="apple-touch-icon" sizes="152x152" href="/icons/apple-icon-152x152.png" />
        <link rel="apple-touch-icon" sizes="180x180" href="/icons/apple-icon-180x180.png" />
        {/* Microsoft */}
        <meta name="msapplication-config" content="/browserconfig.xml" />
        <meta name="msapplication-TileColor" content="#0063a9" />
        <meta name="msapplication-tap-highlight" content="no" />
        {/* Android */}
        <link rel="icon" type="image/png" sizes="192x192" href="/icons/android-icon-192x192.png" />
        {/* Google Analytics */}
        {process.env.NODE_ENV == 'production' && (
          <>
            <script
              async
              src={`https://www.googletagmanager.com/gtag/js?id=${process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS_TID}`}
            ></script>
            <script
              dangerouslySetInnerHTML={{
                __html: `
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());
                gtag('config', '${process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS_TID}');
                `,
              }}
            />
          </>
        )}

        {/* Fonts */}
        <link rel="preconnect" href="//fonts.gstatic.com" />
        <link href="//fonts.googleapis.com/css2?family=Pacifico&display=swap" rel="stylesheet" />
      </Head>
      {children}
    </>
  )
}
