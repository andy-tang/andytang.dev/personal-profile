import React from 'react'
import projects from '../assets/json/projects.json'

import { Box, Button, Card, CardActions, CardContent, CardMedia, Chip, Divider, Grid } from '@material-ui/core'
import styles from '../styles/pages/Projects.module.scss'

interface Project {
  title: string
  subTitle: string
  currentProject?: boolean
  type: string
  preview: string
  badges: { img: string; alt: string }[]
  repo: string
  demo: string
  keywords: {
    [key: string]: string[] | undefined
  }
}

export default function Projects() {
  return (
    <div className={styles.projects}>
      <section className="intro">
        {projects.intro.map((value, key) => (
          <p key={key}>{value}</p>
        ))}
      </section>
      <DevOps />
      <WebApp />
      <section className="planning">
        <h2>Planning / Updating Projects</h2>
        <Divider style={{ margin: '2em 0 3em' }} />
        <p>
          Existing projects that need to be updated for demonstration or new projects that I am planning to implement.
        </p>
        <ul>
          {projects.planning.map((value, key) => (
            <li key={key}>{value}</li>
          ))}
        </ul>
      </section>
    </div>
  )
}

export const DevOps = () => {
  return (
    <section className={styles.devOps}>
      <h2>DevOps</h2>
      <Divider style={{ margin: '2em 0 3em' }} />
      <p>All projects listed on this page (with a demo site) are integrated with CI/CD pipeline.</p>
      <p>The workflow below shows the major stages in our CI/CD process.</p>
      <div className={styles.chart}>
        <img src={require('../assets/images/projects/cicd-pipeline.svg')} alt="cicd-pipeline" />{' '}
      </div>
      <p>Here are the system and service details.</p>
      <table className={styles.details}>
        <tr>
          <th scope="row">Source Code Management</th>
          <td>
            <a href="https://about.gitlab.com/">Gitlab.com</a>
          </td>
        </tr>
        <tr>
          <th scope="row">Automation Service</th>
          <td>
            Self-hosted <a href="https://docs.gitlab.com/runner">GitLab Runner</a>
          </td>
        </tr>
        <tr>
          <th scope="row">Container Service</th>
          <td>
            <a href="https://www.docker.com">Docker</a>
          </td>
        </tr>
        <tr>
          <th scope="row">Container Registry</th>
          <td>
            <a href="https://docs.gitlab.com/ee/user/packages/container_registry">GitLab Container Registry</a>
          </td>
        </tr>
        <tr>
          <th scope="row">Production</th>
          <td>
            Self-hosted single node Kubernetes cluster deployed with <a href="https://k3s.io">K3S</a>
          </td>
        </tr>
      </table>
    </section>
  )
}

export const WebApp = () => {
  const webApp: Project[] = projects.projects.filter(({ type }) => type === 'web-app')

  return (
    <section className={styles.webApp}>
      <h2>Web Applications</h2>
      <Divider style={{ margin: '2em 0 3em' }} />
      <Grid container spacing={2}>
        {webApp.map((project, index) => (
          <Grid item xs={12} md={6} key={index}>
            <ProjectCard project={project} />
          </Grid>
        ))}
      </Grid>
    </section>
  )
}

export const ProjectCard = ({ project }: { project: Project }) => {
  return (
    <Card className={styles.card}>
      <div>
        <Box component="div" overflow="hidden">
          {project.currentProject && (
            <div className={styles.ribbon}>
              <span className={styles.label}>This project</span>
            </div>
          )}
          <CardMedia image={project.preview} title={project.title} />
        </Box>
        <CardContent>
          <div className={styles.title}>{project.title}</div>
          <div className={styles.subTitle}>{project.subTitle}</div>
          <div className={styles.badges}>
            {project.badges.map(({ img, alt }, _index) => (
              <img src={img} alt={alt} key={_index} />
            ))}
          </div>
          <div className={styles.keywords}>
            {Object.entries(project.keywords).map(([key, values]: [string, string[]], _index) => (
              <Grid container key={_index}>
                <Grid item xs={12} md={4} lg={3} className={styles.key}>
                  {key}
                </Grid>
                <Grid item xs={12} md={8} lg={9}>
                  {values.map((value, __index) => (
                    <Chip variant="outlined" color="primary" size="small" label={value} key={__index} />
                  ))}
                </Grid>
              </Grid>
            ))}
          </div>
        </CardContent>
      </div>
      <CardActions>
        <Grid container spacing={2} className={styles.actions}>
          <Grid item xs={12} lg={6}>
            <Button
              size="small"
              variant="contained"
              color="primary"
              fullWidth
              startIcon={<img src={require('../assets/images/common/gitlab-logo.svg')} alt="Gitlab" />}
              onClick={() => window.open(project.repo)}
            >
              View Gitlab
            </Button>
          </Grid>
          <Grid item xs={12} lg={6}>
            <Button
              size="small"
              variant="contained"
              color="primary"
              fullWidth
              startIcon={<img src={require('../assets/images/common/view.svg')} alt="Demo" />}
              onClick={() => window.open(project.demo)}
            >
              View demo
            </Button>
          </Grid>
        </Grid>
      </CardActions>
    </Card>
  )
}
