import { Fragment, useState } from 'react'
import dynamic from 'next/dynamic'
import NodeGeocoder from 'node-geocoder'
import classNames from 'classnames'
import gfm from 'remark-gfm'
import moment from 'moment'
import { byIso as countryByIso } from 'country-code-lookup'
import { groupBy, sortBy } from 'lodash'
import trips from '../assets/json/trips.json'
import intro from '../assets/md/intro.md'

import Flag from 'react-flagkit'
import ReactMarkdown from 'react-markdown'
import { Grid, List, ListItem, ListItemText, Collapse } from '@material-ui/core'
import { ExpandMore } from '@material-ui/icons'
import styles from '../styles/pages/Intro.module.scss'

const TravelMap = dynamic(() => import('../components/Chart/TravelMap'), { ssr: false })
interface VisitedCities {
  [country: string]: { [city: string]: { long: number; lat: number; dates: string[] } }
}

const getCountryName = (iso: string) => {
  try {
    return countryByIso(iso)?.country
  } catch (_e) {
    return iso
  }
}

export async function getStaticProps() {
  const geocoder = NodeGeocoder({ provider: 'google', apiKey: process.env.GOOGLE_GEOCODE_API_KEY })
  const vistedCities: VisitedCities = Object.fromEntries(
    await Promise.all(
      sortBy(Object.entries(groupBy(trips, 'country')), '0').map(async ([country, cities]) => [
        country,
        Object.fromEntries(
          sortBy(
            await Promise.all(
              Object.entries(groupBy(cities, 'city')).map(async ([city, value]) => {
                const result = await geocoder.geocode(`${city}, ${getCountryName(country)}`)
                const { longitude: long = 0, latitude: lat = 0 } = result.length > 0 ? result[0] : {}
                return [city, { long, lat, dates: value.map(({ date }) => date) }]
              })
            ),
            '0'
          )
        ),
      ])
    )
  )

  return {
    props: { vistedCities },
  }
}

export default function Intro({ vistedCities }: { vistedCities: VisitedCities }) {
  return (
    <Grid container direction="column" wrap="nowrap" className={styles.intro}>
      <Grid item>
        {<ReactMarkdown remarkPlugins={[gfm]} children={intro} />}

        <hr />
        <h2>I visited</h2>
        <TravelMap mapData={vistedCities} />
        <VisitedCountries items={vistedCities} />
      </Grid>
    </Grid>
  )
}

const VisitedCountries = ({ items = {} }: { items: VisitedCities }) => {
  const [selected, setSelected] = useState(-1)
  return (
    <List disablePadding className={styles.visitedCountries}>
      {Object.entries(items).map(([country, cities], index) => {
        const _cities = Object.entries(cities)
        return (
          <Fragment key={index}>
            <ListItem button disableGutters dense onClick={() => setSelected(selected === index ? -1 : index)}>
              <Flag country={country} className={styles.flag} />
              <ListItemText
                primary={`${getCountryName(country)} - ${_cities.length} ${_cities.length > 1 ? 'cities' : 'city'}`}
              />
              <ExpandMore className={classNames(styles.arrow, { [styles.active]: selected === index })} />
            </ListItem>
            <Collapse in={selected === index} timeout="auto" unmountOnExit>
              {_cities.map(([city, { dates }]) => (
                <Grid container className={styles.cities} key={city}>
                  <Grid item xs={4} lg={2}>
                    {city}
                  </Grid>
                  <Grid item xs={8} lg={10} className={styles.dates}>
                    {dates.map((date) => moment(date).format('MMM YYYY')).join(', ')}
                  </Grid>
                </Grid>
              ))}
            </Collapse>
          </Fragment>
        )
      })}
    </List>
  )
}
