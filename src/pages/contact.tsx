import { LegacyRef, useRef } from 'react'
import { useDispatch } from 'react-redux'
import { useMutation } from 'react-query'
import { useForm, Controller } from 'react-hook-form'
import { useSnackbar } from 'notistack'
import parsePhoneNumber from 'libphonenumber-js'
import { apiSendContactForm } from '../apis'
import { openBackdrop } from '../stores'
import personalInfo from '../assets/json/personal.json'

import { Avatar, Button, Grid, List, ListItem, ListItemAvatar, ListItemText, TextField } from '@material-ui/core'
import ReCAPTCHA from 'react-google-recaptcha'
import styles from '../styles/pages/Contact.module.scss'

const contacts: { icon: string; label: string; value: any }[] = [
  {
    icon: require('../assets/images/contact/email.svg'),
    label: 'Email',
    value: <a href={`mailto:${personalInfo.contact.email}`}>{personalInfo.contact.email}</a>,
  },
  {
    icon: require('../assets/images/contact/phone.svg'),
    label: 'Phone',
    value: (
      <span>{parsePhoneNumber(personalInfo.contact.phone)?.formatInternational() ?? personalInfo.contact.phone}</span>
    ),
  },
  {
    icon: require('../assets/images/contact/signal.svg'),
    label: 'Signal',
    value: (
      <span>{parsePhoneNumber(personalInfo.contact.signal)?.formatInternational() ?? personalInfo.contact.signal}</span>
    ),
  },
  {
    icon: require('../assets/images/contact/whatsapp.svg'),
    label: 'WhatsApp',
    value: (
      <a
        href={`https://wa.me/${personalInfo.contact.whatsapp.num}?text=${encodeURIComponent(
          personalInfo.contact.whatsapp.text
        )}`}
      >
        {parsePhoneNumber(personalInfo.contact.whatsapp.num)?.formatInternational() ??
          personalInfo.contact.whatsapp.num}
      </a>
    ),
  },
]

export default function Contact() {
  const dispatch = useDispatch()
  const recaptchaRef = useRef<ReCAPTCHA>()
  const { enqueueSnackbar } = useSnackbar()
  const { control, handleSubmit, errors, reset } = useForm({
    defaultValues: {
      name: '',
      email: '',
      phone: '',
      message: '',
    },
  })
  const { mutate: sendContactForm } = useMutation(apiSendContactForm, {
    onMutate() {
      dispatch(openBackdrop(true))
    },
    onSuccess() {
      enqueueSnackbar('Done! We got your message.', { variant: 'success' })
      reset()
    },
    onError() {
      enqueueSnackbar('Opps! Something went wrong.', { variant: 'error' })
    },
    onSettled() {
      recaptchaRef?.current?.reset()
      dispatch(openBackdrop(false))
    },
  })

  const onSubmit = async (data: { name: string; email: string; phone?: number; message: string }) => {
    if (recaptchaRef?.current) {
      const token = await recaptchaRef?.current.executeAsync()
      sendContactForm({ ...data, token })
    }
  }

  return (
    <div className={styles.contact}>
      <p>Feel free to get in touch with me directly at any time.</p>
      <List>
        {contacts.map(({ icon, label, value }, index) => (
          <ListItem key={index}>
            <ListItemAvatar>
              <Avatar alt={label} src={icon} />
            </ListItemAvatar>
            <ListItemText primary={label} secondary={value} />
          </ListItem>
        ))}
      </List>
      <h2>Leave a message</h2>
      <p>Or you can leave a message here, I will get back to you soon.</p>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Grid container spacing={2} className={styles.contactForm}>
          <Grid item xs={12} md={6}>
            <Controller
              name="name"
              control={control}
              rules={{
                required: {
                  value: true,
                  message: 'Who may i reply to?',
                },
              }}
              render={(props) => (
                <TextField
                  {...props}
                  label="Your Name"
                  type="text"
                  onChange={(e) => props.onChange(e.target.value)}
                  error={!!errors.name}
                  helperText={errors.name?.message}
                  variant="outlined"
                  fullWidth
                  size="small"
                />
              )}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <Controller
              name="email"
              control={control}
              rules={{
                required: {
                  value: true,
                  message: 'Please provide me an email address for replying your message.',
                },
                pattern: {
                  value: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
                  message: 'Please provide a valid email address.',
                },
              }}
              render={(props) => (
                <TextField
                  {...props}
                  label="Email"
                  type="email"
                  onChange={(e) => props.onChange(e.target.value)}
                  error={!!errors.email}
                  helperText={errors.email?.message}
                  variant="outlined"
                  fullWidth
                  size="small"
                />
              )}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <Controller
              name="phone"
              control={control}
              rules={{
                pattern: {
                  value: /^[4-9]\d{7}$/,
                  message: 'Please provide a valid phone number.',
                },
              }}
              render={(props) => (
                <TextField
                  {...props}
                  label="Phone"
                  type="tel"
                  onChange={(e) => props.onChange(e.target.value)}
                  error={!!errors.phone}
                  helperText={errors.phone?.message}
                  variant="outlined"
                  fullWidth
                  size="small"
                />
              )}
            />
          </Grid>
          <Grid item xs={12}>
            <Controller
              name="message"
              control={control}
              rules={{
                required: {
                  value: true,
                  message: 'Please input your message.',
                },
              }}
              render={(props) => (
                <TextField
                  {...props}
                  label="Message"
                  onChange={(e) => props.onChange(e.target.value)}
                  error={!!errors.message}
                  helperText={errors.message?.message}
                  multiline
                  rows={4}
                  variant="outlined"
                  fullWidth
                  size="small"
                />
              )}
            />
          </Grid>
          <Grid item xs={12} style={{ textAlign: 'center' }} className={styles.recaptchaTos}>
            This page is protected by reCAPTCHA and the Google{' '}
            <a href="https://policies.google.com/privacy">Privacy Policy</a> and{' '}
            <a href="https://policies.google.com/terms">Terms of Service</a> apply.
          </Grid>
          <Grid item xs={12} style={{ textAlign: 'center' }}>
            <Button type="submit" variant="contained" color="primary">
              Submit
            </Button>
          </Grid>
        </Grid>
        <ReCAPTCHA
          ref={recaptchaRef as unknown as LegacyRef<ReCAPTCHA>}
          size="invisible"
          sitekey={process.env.NEXT_PUBLIC_RECAPTCHA_SITE_KEY || ''}
        />
      </form>
    </div>
  )
}
