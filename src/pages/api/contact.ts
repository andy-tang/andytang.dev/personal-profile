import type { NextApiRequest, NextApiResponse } from 'next'

import { validate as isEmail } from 'email-validator'
import { htmlToText } from 'html-to-text'
import { ReasonPhrases, StatusCodes } from 'http-status-codes'
import moment from 'moment-timezone'
import nodemailer from 'nodemailer'
import { ApiResp, createResp } from './_index'
import { apiVerifyRecaptcha } from '../../apis'
import systemConfig from '../../assets/json/system-config.json'

const transporter = nodemailer.createTransport({
  host: process.env.SMTP_HOST,
  port: parseInt(process.env.SMTP_PORT ?? ''),
  secure: process.env.SMTP_TSL?.toLowerCase() === 'true',
  auth: {
    user: process.env.SMTP_USER,
    pass: process.env.SMTP_PWD,
  },
})

const validateContactForm = async ({
  name,
  email,
  message,
  token,
}: {
  name: string
  email: string
  message: string
  token: string
}) => {
  const errors = []
  if (!name) errors.push(['name', 'Please input name.'])
  if (!email) errors.push(['email', 'Please input email.'])
  else if (!isEmail(email)) errors.push(['email', 'Please input a valid email.'])
  if (!message) errors.push(['message', 'Please input message.'])
  if (!token) errors.push(['token', 'Please provide token.'])
  else {
    const { success }: { success: boolean } = (await apiVerifyRecaptcha({ response: token })).data
    if (!success) errors.push(['token', 'Please provide a valid token.'])
  }
  return errors
}

const emailTemplate = ({
  name,
  email,
  phone,
  message,
}: {
  name: string
  email: string
  phone: string
  message: string
}) => {
  return `
  <div>
  <p>Hi ${name},</p>
  <p>
  Thank you for getting in touch with me! I will get back to you as soon as
  possible.
  </p>
  <p>Have a nice day!</p>
  <p>
    All the best,<br />
    Andy Tang
  </p>
  <p>** This is an automated message, please do not reply directly to this email. **</p>
  <hr />
  <h4>CONTACT FORM DETAILS</h4>

  <p><b>Name:</b><br />${name}</p>
  <p><b>Email:</b><br />${email}</p>
  <p><b>Phone Number:</b><br />${phone || 'N/A'}</p>
  <p><b>Message:</b><br />${message}</p>
  <p>
    <b>Submission time:</b><br />${moment().tz('Asia/Hong_Kong').format('MMMM Do YYYY, h:mm:ss a')}
  </p>
  </div>
`
}

export default async (req: NextApiRequest, res: NextApiResponse<ApiResp>) => {
  if (req.method !== 'POST') {
    return res
      .status(StatusCodes.METHOD_NOT_ALLOWED)
      .json(createResp({ error: { code: StatusCodes.METHOD_NOT_ALLOWED, msg: ReasonPhrases.METHOD_NOT_ALLOWED } }))
  }
  const errors = await validateContactForm(req.body)
  if (errors.length > 0) {
    return res
      .status(StatusCodes.BAD_REQUEST)
      .json(createResp({ error: { code: StatusCodes.BAD_REQUEST, msg: ReasonPhrases.BAD_REQUEST, details: errors } }))
  }

  // Send confirm email to guest
  try {
    await transporter.sendMail({
      from: systemConfig.contactFromEmail.from,
      to: req.body.email,
      bcc: systemConfig.contactFromEmail.bcc,
      replyTo: systemConfig.contactFromEmail.replyTo,
      subject: systemConfig.contactFromEmail.subject,
      text: htmlToText(emailTemplate(req.body)),
      html: emailTemplate(req.body),
    })
  } catch (e) {
    return res.status(StatusCodes.SERVICE_UNAVAILABLE).json(
      createResp({
        error: { code: StatusCodes.SERVICE_UNAVAILABLE, msg: ReasonPhrases.SERVICE_UNAVAILABLE, details: e.message },
      })
    )
  }
  res.status(200).json(createResp({}))
}
