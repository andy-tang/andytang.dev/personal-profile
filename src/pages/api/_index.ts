export type ApiResp = {
  data?: object
  error?: { code: number; msg: string; details?: object }
  timestamp: number
}

export const createResp = ({
  data,
  error,
}: {
  data?: object
  error?: { code: number; msg: string; details?: object }
}) => {
  return { data, error, timestamp: Math.floor(Date.now() / 1000) }
}
