import styles from '../styles/pages/About.module.scss'

export default function About() {
  return (
    <div className={styles.about}>
      <div>
        <section id="acknowledgements">
          <h2>Acknowledgements</h2>
          <ul>
            <li>
              Website layout and colour scheme inspired by{' '}
              <a href="https://html5up.net/future-imperfect">Future Imperfect</a> by{' '}
              <a href="https://github.com/ajlkn">@ajlkn</a> and <a href="https://mldangelo.com/">Michael D'Angelo</a> by{' '}
              <a href="https://github.com/mldangelo">@mldangelo</a>
            </li>
            <li>
              Social media icons made by{' '}
              <a href="https://www.freepik.com" title="Freepik">
                Freepik
              </a>{' '}
              from{' '}
              <a href="https://www.flaticon.com/" title="Flaticon">
                www.flaticon.com
              </a>
            </li>
          </ul>
        </section>

        <section id="terms-of-use">
          <hr />
          <h2>Terms of Use</h2>

          <h3>Content</h3>
          <p>
            All text, graphics, user interfaces, visual interfaces, photographs, trademarks, logos, sounds, music,
            artwork and computer code (collectively, "Content"), including but not limited to the design, structure,
            selection, coordination, expression, "look and feel" and arrangement of such Content, contained on this
            Website is owned, controlled or licensed by or to andytang.dev, and is protected by trade dress, copyright,
            patent and trademark laws, and various other intellectual property rights and unfair competition laws.
          </p>
          <p>
            Except as expressly provided in these Terms of Use, no part of this Website and no Content may be copied,
            reproduced, republished, uploaded, posted, publicly displayed, encoded, translated, transmitted or
            distributed in any way (including "mirroring") to any other computer, server, Web site or other medium for
            publication or distribution or for any commercial enterprise, without andytang.dev's express prior written
            consent.
          </p>
          <p>
            You may use information on Intro and Resume page for recruitment purpose, provided that you (1) not remove
            any proprietary notice language in all copies of such documents, (2) do not copy or post such information on
            any networked computer or broadcast it in any media, (3) make no modifications to any such information, and
            (4) not make any additional representations or warranties relating to such documents.
          </p>

          <h3>Source Code</h3>
          <p>
            The published source code of this Website, located at{' '}
            <a href="https://gitlab.com/andy-tang/andytang.dev/personal-profile">
              https://gitlab.com/andy-tang/andytang.dev/personal-profile
            </a>
            , is licensed with MIT license.
          </p>
          <p>
            Please refer to the{' '}
            <a href="https://gitlab.com/andy-tang/andytang.dev/personal-profile/-/raw/master/LICENSE">license file</a> for
            details.
          </p>
        </section>

        <section id="privacy-policy">
          <hr />
          <h2>Privacy Policy</h2>
          <p>
            We are committed to protecting your personal information and your right to privacy. If you have any
            questions or concerns about our policy, or our practices with regards to your personal information, please
            contact us.
          </p>
          <p>
            This Privacy Policy governs the privacy policies and practices of this Website, located at andytang.dev.
            Please read our Privacy Policy carefully as it will help you make informed decisions about sharing your
            personal information with us.
          </p>

          <h3>Information We Collect</h3>
          <p>
            As a Visitor, you can browse this Website to find out more about this Website. You are not required to
            provide us with any personal information as a Visitor.
          </p>

          <h3>Information You Provide to Us</h3>
          <p>
            We collect your personal information when you express an interest in obtaining information about us or our
            products and services, or otherwise contacting us.
          </p>
          <p>
            Generally, you control the amount and type of information you provide to us when using this Website. The
            personal information that we collect depends on the context of your interaction with us and the Website, the
            choices you make and the products and features you use. The personal information we collect can include the
            following:
          </p>
          <ul>
            <li>Name</li>
            <li>Email Address</li>
            <li>Phone Number</li>
            <li>Contact Data</li>
          </ul>
        </section>

        <section id="automatically-collected-information">
          <hr />
          <h2>Automatically Collected Information</h2>
          <p>
            When you use this Website, we automatically collect certain computer information by the interaction of your
            mobile phone or web browser with this Website. Such information is typically considered non-personal
            information. We also collect the following:
          </p>
          <h3>Cookies</h3>
          <p>
            This Website itself do not use cookies to identify users personally. However, some services or plugins we
            used on this Website may collect cookies for web security and analytic purposes. Those cookies, which are
            third party cookies, do not provide us with information that can be used to identify a visitor on this
            Website.
          </p>
          <h4>reCAPTCHA</h4>
          <p>
            ReCAPTCHA is a web service offered by Google that collects information which can be used by Google to
            identify a visitor so as to determine if the actions on this Website are actually human.
          </p>
          <p>
            For more information on the privacy practices of Google, please visit the{' '}
            <a href="https://policies.google.com/privacy">Google Privacy page</a>.
          </p>
          <h4>Google Analytics</h4>
          <p>
            Google Analytics is a web analytics service offered by Google that tracks and reports website traffic.
            Google uses the data collected to track and monitor the use of our Service. This data is shared with other
            Google services. Google may use the collected data to contextualize and personalize the ads of its own
            advertising network.
          </p>
          <p>
            You can opt-out of having made your activity on the Service available to Google Analytics by installing the
            Google Analytics opt-out browser add-on. The add-on prevents the Google Analytics JavaScript (ga.js,
            analytics.js, and dc.js) from sharing information with Google Analytics about visits activity.
          </p>
          <p>
            For more information on the privacy practices of Google, please visit the{' '}
            <a href="https://policies.google.com/privacy">Google Privacy page</a>.
          </p>{' '}
        </section>
      </div>
    </div>
  )
}
