import React from 'react'
import { useRouter } from 'next/router'
import { createUseStyles } from 'react-jss'
import { random } from 'lodash'
import excuses from '../assets/json/excuses.json'

import { Button } from '@material-ui/core'

const useStyles = createUseStyles({
  notFound: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    height: '100%',
  },
  nothing: {
    position: 'relative',
    width: '100%',

    '& .wrapper': {
      position: 'absolute',
      left: 0,

      '& p': {
        color: '#ffffff',
      },
    },
  },
  message: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: '2.5em',

    '& img': {
      width: '10em',
    },
    '& button': {
      margin: '2em 0',
    },
    '& > *': {
      textAlign: 'center',
    },
    '& .desc': {
      fontSize: '1.25em',
      fontWeight: '800',
      letterSpacing: '0.1em',
      lineHeight: '1.65',
      margin: '1em 0',
      textAlign: 'center',
      textTransform: 'unset',
    },
    '& .excuse': {
      wordBreak: 'break-word',
      fontSize: '1.15em',
      letterSpacing: '1px',
      color: '#3a3a3a',
    },
  },
})

const PageNotFound = () => {
  const router = useRouter()
  const styles = useStyles()
  const excuse = excuses[random(excuses.length - 1)]

  return (
    <div className={styles.notFound}>
      <div className={styles.nothing}>
        <div className="wrapper">
          <p>You found me out! :O</p>
          <p>But there is really nothing!</p>
        </div>
      </div>

      <div className={styles.message}>
        <img src={require('../assets/images/error/error-404.svg')} alt="404" />
        <div className="desc">It seems like an error</div>
        <div className="excuse">{excuse}</div>
        <Button variant="contained" color="primary" onClick={() => router.push('/')}>
          Back to Home
        </Button>
      </div>
    </div>
  )
}

export default PageNotFound
