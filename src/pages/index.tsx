import React from 'react'
import systemConfig from '../assets/json/system-config.json'

import { Avatar, Divider, Grid } from '@material-ui/core'
import withWidth, { isWidthUp } from '@material-ui/core/withWidth'
import { Breakpoint } from '@material-ui/core/styles/createBreakpoints'
import { ActionButtons, SocialMediaBar } from '../components'
import styles from '../styles/pages/Home.module.scss'

export default withWidth(styles)(function Home({ width }: { width: Breakpoint }) {
  return (
    <div className={styles.home}>
      <Grid container alignContent="center" justify="center" className={styles.billboard}>
        <Grid item className={styles.wrapper}>
          <Grid container direction={isWidthUp('md', width) ? 'row' : 'column-reverse'}>
            <Grid item xs={12} md={6} className={styles.message}>
              <h1>
                <p>Hello, my name is</p>
                <p>
                  <mark>{systemConfig.site.title}</mark>.
                </p>
              </h1>
              <Divider />
              <p>Welcome to my personal website.</p>
              <ActionButtons />
            </Grid>
            <Grid item xs={12} md={6} className={styles.avatar}>
              <Avatar
                variant="rounded"
                src={require('../assets/images/avatar.svg')}
                alt="avatar"
                className={styles.avatar}
              />
            </Grid>
          </Grid>
        </Grid>
      </Grid>

      <div className={styles.social}>
        <SocialMediaBar />
      </div>
    </div>
  )
})
